
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

function isVideo(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
    case 'ogg':
    case 'avi':
    case 'mov':
        return true;
    }
    return false;
}


jQuery(function() {
  jQuery(".opti-gallery-image").each(function() {
    var that = this
    var image = jQuery(this).children().text();
    var path = "/sites/www.mannapro.com/files/webform/optigut2018/" + image;

    if(isVideo(image)){
        var new_name = image.split('.')
        new_name.pop()
        var final_video_name = new_name.join('')
        var possible_url = 'https://res.cloudinary.com/optigut/video/upload/v1522877739/mannapro/' + final_video_name + '.gif'

        jQuery.ajax({
            type: 'HEAD',
            url: possible_url,
            success: function() {
                console.log('in success')
                jQuery(this).css('background-image', 'url(' + possible_url + ')');
            },
            error: function() {
                console.log('in err')
                jQuery.ajax({
                type: "POST",
                url: 'https://cloudinarymannagrow.herokuapp.com/upload_video',
                data: {'video_url': 'https://www.mannapro.com' + path},
                success: function(){
                    jQuery(that).css('background-image', 'url(' + possible_url + ')');
                }
            }); 
            }            
        });
        }
        jQuery(this).css('background-image', 'url(' + path + ')');
    });
});