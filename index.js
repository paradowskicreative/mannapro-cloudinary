const express = require('express')
const app = express()
const cloudinary = require('cloudinary')
const bodyParser = require('body-parser')
var cors = require('cors')

app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

// require('dotenv').config()

const cloud_name = process.env.CLOUDINARY_CLOUD_NAME
const api_key = process.env.CLOUDINARY_API_KEY
const api_secret = process.env.CLOUDINARY_API_SECRET


cloudinary.config({
  cloud_name: cloud_name,
  api_key: api_key,
  api_secret: api_secret
})

app.post('/upload_video', cors(), (req, res) => {
  console.log(req.body)
  var vid_url = req.body.video_url
  var split_url = vid_url.split('/')
  var video_name = split_url[split_url.length - 1]
  var video_name_split = video_name.split('.')
  video_name_split.pop()
  var final_video_name = video_name_split.join('')
  cloudinary.v2.uploader.upload(req.body.video_url, {
    resource_type: "video",
    upload_preset: "optigut",
    folder: "mannapro",
    public_id: final_video_name
  },
  function(err, result){
    console.log(result)
    res.send(result)
  })

})

app.listen(process.env.PORT || 5000, () => console.log('listening on 5000'))
